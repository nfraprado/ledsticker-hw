#############
ledsticker-hw
#############

About Ledsticker
================

The Ledsticker project aims to make a LED matrix controllable through USB. The
objective is to be able to glue the LED matrix to the back of a notebook lid and
program it to show static or animated drawings (and can even be generated from a
program), effectively turning it into a *LED sticker*. For the project's
backstory, see my `blog post`_.

This is achieved through both software and hardware components. This repository
concerns itself with the hardware side. For the software side, see
https://codeberg.org/nfraprado/ledsticker instead.

.. image:: img/ledsticker_final_resized_480.gif

Ledsticker hardware
===================

The hardware is basically composed of three components:

* 8x8 LED single-color matrix
* MAX7219 IC
* MCP2210 IC

The LED matrix is where the programmed drawings show up. Each of the 64 LEDs can
be turned on or off to effectively form an image, with each LED acting as a
pixel.

The MAX7219 is an integrated circuit (IC) that controls the LED matrix. It has
an internal memory with the current state of each pixel, and is responsible for
driving each of the matrix's LEDs accordingly. It is controlled through an SPI
bus, where it receives updates for the matrix as well as commands (like
brightness regulation).

The MCP2210 is another IC and it serves as the USB-to-SPI converter, so that all
data can be sent from the computer through USB and it converts to SPI so that it
reaches the MAX7219.

These are the main components. There are other few passive components required
to make the circuit work (capacitors, resistors and a crystal) as well as a
USB connector.

To make it all compact and easy to use, a custom Printed Circuit Board (PCB) is
used to connect all of these components.

About this repo
===============

This repository contains all the relevant information for the hardware part of
the project, that is:

* Kicad project for the PCB
* Bill of Materials (BOM)

The board can be directly ordered from OSHPark from
https://oshpark.com/shared_projects/MHbtuh9G.

.. image:: img/boards.jpg

Bill of Materials (BOM)
=======================

======== ========================================== ===================
Quantity Component                                  Where I bought from
======== ========================================== ===================
1        8x8 single-color led matrix (1088AS model) `Cinestec (Brazillian store) <ledmatrix_>`__
1        MAX7219 (24-pin DIP package)               `Cinestec (Brazillian store) <ledmatrix_>`__
1        MCP2210 (20-lead SOIC package)             `Digi-Key <mcp2210_>`__
1        USB B-Mini female connector (DS1104 model) `Mult Comercial (Brazillian store) <usb_>`__
1        12.000 MHz crystal                         `Mult Comercial (Brazillian store) <crystal_>`__
1        24-pin IC socket                           `Mult Comercial (Brazillian store) <socket_>`__
2        8-pin female header                        `Mult Comercial (Brazillian store) <header_>`__
1        10uF electrolytic capacitor
1        0.1uF disc capacitor
1        0.47uF capacitor
1        10kΩ resistor
1        390Ω resistor
======== ========================================== ===================

All the components next to the PCB:

.. image:: img/components.jpg

Soldering
=========

When soldering through-hole components, it's best to solder the shorter ones
first.

Also, when I soldered the components, I left the SMD ones for the end, but that
was a bad decision because the USB B-Mini connector is already hard to solder
(see Caveats_ below), and soldering it after the 24-pin IC socket and 8-pin
female headers left very little space to use the soldering iron on it.

So the following is what I think is the best order to solder in:

1. resistors
2. crystal
3. MCP2210
4. USB connector
5. pin headers
6. IC socket
7. disc capacitor
8. 0.47uF capacitor
9. electrolytic capacitor

Caveats
=======

The USB B-Mini connector was by far the most difficult component to solder, in
great part because the trail on the PCB for its 5 leads are way too short. If I
were to make a v2 of the board I'd increase those in the footprint, but as I
managed to solder it, I'm very happy with the result and not planning on making
a v2.

License
=======

`CC BY-SA 4.0`_

.. _blog post: https://nfraprado.net/post/ledsticker-my-first-holistic-sw-hw-project.html
.. _ledmatrix: http://www.cinestec.com.br/product_info.php?products_id=22835
.. _mcp2210: https://www.digikey.com/en/products/detail/microchip-technology/MCP2210-I-SO/2835086
.. _usb: https://www.multcomercial.com.br/conector-mini-usb-femea-tipo-b-em-smd-5-vias-ds1104-b60sr.html
.. _crystal: https://www.multcomercial.com.br/12-000mhz-hc-49-s-cod-loja-3300.html
.. _socket: https://www.multcomercial.com.br/soquete-estampado-slim-mac24-24-pinos-cod-loja-4959.html
.. _header: https://www.multcomercial.com.br/soquete-torneado-cpt16-16-pinos-cod-loja-140-a-144.html

.. _CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/
